class RoverOutOfBoundError < StandardError
end

Plateau = Struct.new(:max_x, :max_y)

Position = Struct.new(:x, :y)

class Rover
  attr_reader :position, :direction, :commands

  DIRECTION_MAP = {
    'N' => :north,
    'W' => :west,
    'S' => :south,
    'E' => :east,
  }

  SYMBOL_TO_DIRECTION_MAP = DIRECTION_MAP.invert

  COMMAND_MAP = {
    'L' => :left,
    'R' => :right,
    'M' => :move,
  }

  DIRECTION_TRANSITION_MAP = {
    :north => {
      :left  => :west,
      :right => :east,
    },
    :east  => {
      :left  => :north,
      :right => :south,
    },
    :south => {
      :left  => :east,
      :right => :west,
    },
    :west  => {
      :left  => :south,
      :right => :north,
    },
  }

  def initialize(position, direction, commands)
    @position = position
    @direction = DIRECTION_MAP[direction]
    @commands = commands.chars.collect { |c| COMMAND_MAP[c] }
  end

  def move_on_plateau(plateau)
    commands.each { |command|
      case command
      when :left
        turn :left
      when :right
        turn :right
      when :move
        move_forward plateau.max_x, plateau.max_y
      end
    }
  end

  def to_s
    "#{position.x} #{position.y} #{SYMBOL_TO_DIRECTION_MAP[direction]}"
  end

  private
  def turn(move)
    @direction = DIRECTION_TRANSITION_MAP[@direction][move]
  end

  def move_forward(max_x, max_y)
    delta_x = case @direction
              when :east
                1
              when :west
                -1
              else
                0
              end
    delta_y = case @direction
              when :north
                1
              when :south
                -1
              else
                0
              end

    @position.x += delta_x
    @position.y += delta_y

    raise RoverOutOfBoundError.new unless (0..max_x).include? @position.x
    raise RoverOutOfBoundError.new unless (0..max_y).include? @position.y
  end
end
