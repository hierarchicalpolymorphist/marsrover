require_relative 'rover'

class ParserError < StandardError  # TODO: Is this the right base exception?
  def initialize(line)
    @line = line
  end

  def to_s
    "On line: #{@line}"
  end
end

class NoCoordinateError < ParserError
end

class MalformedCoordinatesError < ParserError
end

class MalformedRoverInitialStateError < ParserError
end

class MalformedRoverCommandLineError < ParserError
end

DIRECTIONS = ['N', 'S', 'E', 'W']

COMMANDS = ['L', 'R', 'M']

class RoverCommandFileParser
  attr_reader :rovers, :plateau

  def initialize(lines)
    @lines = lines
    @rovers = []
    @plateau = nil
  end

  def parse
    parse_top_right_coordinates
    parse_rovers @lines[1..-1]  # ruby .. range is right-closed (inclusive)
  end

  def parse_rovers(lines)
    @rovers = lines.each_slice(2).collect { |robot_def|
      initial_state_line, command_line = robot_def
      parse_rover initial_state_line, command_line
    }
  end

  def parse_rover(initial_state_line, command_line)
    components = initial_state_line.split
    raise MalformedRoverInitialStateError.new(initial_state_line) if components.size != 3

    x, y, direction = components

    begin
      x = Integer(x)
      y = Integer(y)
    rescue ArgumentError
      raise MalformedRoverInitialStateError.new(initial_state_line)
    end

    raise MalformedRoverInitialStateError.new(initial_state_line) if not DIRECTIONS.include? direction

    raise MalformedRoverCommandLineError.new(command_line) if command_line.nil?

    illegal_commands = command_line.chars.select { |c| not COMMANDS.include? c }
    raise MalformedRoverCommandLineError.new(command_line) if illegal_commands.size > 0
    Rover.new(Position.new(x, y), direction, command_line)
  end

  def parse_top_right_coordinates
    first_line = @lines[0]
    raise NoCoordinateError.new(first_line) if first_line.nil?

    coordinates = first_line.split
    raise MalformedCoordinatesError.new(first_line) if coordinates.size != 2

    x, y = coordinates

    begin
      x = Integer(x)
      y = Integer(y)
    rescue ArgumentError
      raise MalformedCoordinatesError.new(first_line)
    end

    @plateau = Plateau.new(x, y)
  end
end
