require_relative 'parser'

def read_input_file(filename)
  File.open(filename, 'r') do |infile|
    infile.read.lines.collect &:chomp
  end
end

parser = RoverCommandFileParser.new(read_input_file ARGV[0])
parser.parse
plateau = parser.plateau
rovers = parser.rovers

rovers.each { |rover|
  rover.move_on_plateau plateau
  puts rover.to_s
}
