require 'rover.rb'

describe Rover do
  describe ".move_on_plateau" do
    it "should throw RoverOutOfBoundError if rover moves out of plateau on X axis" do
      rover = Rover.new(Position.new(1, 1), 'E', 'MMM')
      expect {
        rover.move_on_plateau(Plateau.new(3, 3))
      }.to raise_error(RoverOutOfBoundError)
    end

    it "should throw RoverOutOfBoundError if rover moves out of plateau on -X axis" do
      rover = Rover.new(Position.new(1, 1), 'W', 'MM')
      expect {
        rover.move_on_plateau(Plateau.new(3, 3))
      }.to raise_error(RoverOutOfBoundError)
    end

    it "should throw RoverOutOfBoundError if rover moves out of plateau on Y axis" do
      rover = Rover.new(Position.new(1, 1), 'N', 'MMM')
      expect {
        rover.move_on_plateau(Plateau.new(3, 3))
      }.to raise_error(RoverOutOfBoundError)
    end

    it "should throw RoverOutOfBoundError if rover moves out of plateau on -Y axis" do
      rover = Rover.new(Position.new(1, 1), 'S', 'MM')
      expect {
        rover.move_on_plateau(Plateau.new(3, 3))
      }.to raise_error(RoverOutOfBoundError)
    end

    it "should stay at the same place if rotating 360 deg" do
      rover = Rover.new(Position.new(1, 1), 'N', 'LLLL')
      rover.move_on_plateau(Plateau.new(3, 3))
      expect(rover.direction).to eq(:north)
      expect(rover.position).to eq(Position.new(1, 1))
    end

    it "can move on the X boundary" do
      rover = Rover.new(Position.new(1, 1), 'E', 'MM')
      rover.move_on_plateau(Plateau.new(3, 3))
      expect(rover.position).to eq(Position.new(3, 1))
    end

    it "can move on the Y boundary" do
      rover = Rover.new(Position.new(1, 1), 'N', 'MM')
      rover.move_on_plateau(Plateau.new(3, 3))
      expect(rover.position).to eq(Position.new(1, 3))
    end

    it "can move to (max-x, max-y)" do
      rover = Rover.new(Position.new(1, 1), 'N', 'MMRMM')
      rover.move_on_plateau(Plateau.new(3, 3))
      expect(rover.position).to eq(Position.new(3, 3))
    end
  end

  describe ".to_s" do
    it "returns the current state of the rover" do
      rover = Rover.new(Position.new(1, 3), "N", "")
      expect(rover.to_s).to eq("1 3 N")
    end
  end
end
