require 'parser.rb'

describe RoverCommandFileParser do
  describe ".parse_top_right_coordinates" do
    it "errors when no lines given" do
      parser = RoverCommandFileParser.new([])
      expect {
        parser.parse_top_right_coordinates
      }.to raise_error(NoCoordinateError)
    end

    it "errors when coordinates line has less than 2 components" do
      parser = RoverCommandFileParser.new(['5'])
      expect {
        parser.parse_top_right_coordinates
      }.to raise_error(MalformedCoordinatesError)
    end

    it "errors when coordinates line has more than 2 components" do
      parser = RoverCommandFileParser.new(['5 10 15'])
      expect {
        parser.parse_top_right_coordinates
      }.to raise_error(MalformedCoordinatesError)
    end

    it "errors when coordinates are malformed" do
      parser = RoverCommandFileParser.new(['5 x'])
      expect {
        parser.parse_top_right_coordinates
      }.to raise_error(MalformedCoordinatesError)
    end

    it "sets @coordinates if input format is right" do
      parser = RoverCommandFileParser.new(['5 5'])
      parser.parse_top_right_coordinates
      expect(parser.plateau.max_x).to eq(5)
      expect(parser.plateau.max_y).to eq(5)
    end
  end

  describe ".parse_rover" do
    it "errors when initial state line does not have components" do
      parser = RoverCommandFileParser.new([])
      expect {
        parser.parse_rover('', 'W')
      }.to raise_error(MalformedRoverInitialStateError)
    end

    it "errors when initial state line does not have exactly 3 components" do
      parser = RoverCommandFileParser.new([])
      expect {
        parser.parse_rover('3 4 N 1', 'W')
      }.to raise_error(MalformedRoverInitialStateError)
    end

    it "errors when initial state line does not have a coordinate" do
      parser = RoverCommandFileParser.new([])
      expect {
        parser.parse_rover('3 x N', 'W')
      }.to raise_error(MalformedRoverInitialStateError)
    end

    it "errors when initial state line does not have a direction" do
      parser = RoverCommandFileParser.new([])
      expect {
        parser.parse_rover('3 5', 'W')
      }.to raise_error(MalformedRoverInitialStateError)
    end

    it "errors when initial state line has an invalid direction" do
      parser = RoverCommandFileParser.new([])
      expect {
        parser.parse_rover('4 5 x', 'W')
      }.to raise_error(MalformedRoverInitialStateError)
    end

    it "errors when commands line is empty" do
      parser = RoverCommandFileParser.new([])
      expect {
        parser.parse_rover('4 5 W', nil)
      }.to raise_error(MalformedRoverCommandLineError)
    end

    it "errors when command line contains invalid direction" do
      parser = RoverCommandFileParser.new([])
      expect {
        parser.parse_rover('4 5 N', 'LMRMMMRMm')
      }.to raise_error(MalformedRoverCommandLineError)
    end

    it "returns a rover object when it can be parsed" do
      parser = RoverCommandFileParser.new([])
      rover = parser.parse_rover('4 5 N', 'LMRMMMRM')
      expect(rover.position).to eq(Position.new(4, 5))
    end
  end

  describe ".parse_rovers" do
    it "returns a single rover if only one definition is provided" do
      parser = RoverCommandFileParser.new([])
      parser.parse_rovers(["1 1 N", "LMRM"])
      expect(parser.rovers.size).to eq(1)
      expect(parser.rovers[0].position).to eq(Position.new(1, 1))
      expect(parser.rovers[0].direction).to eq(:north)
      expect(parser.rovers[0].commands).to eq([:left, :move, :right, :move])
    end

    it "returns multiple rovers if multiple definitions are provided" do
      parser = RoverCommandFileParser.new([])
      parser.parse_rovers(["1 1 N", "LMRM", "2 2 E", "MM"])
      expect(parser.rovers.size).to eq(2)
    end
  end

  describe ".parse" do
    it "parses input lines into the world and rovers" do
      parser = RoverCommandFileParser.new(['5 5', '1 1 N', 'MM', '2 3 E', 'LMR'])
      parser.parse
      expect(parser.rovers.size).to eq(2)
      expect(parser.plateau.max_x).to eq(5)
      expect(parser.plateau.max_y).to eq(5)
    end
  end
end
